package tf.bug.monadmachines.item

import net.fabricmc.fabric.api.item.v1.FabricItemSettings
import net.minecraft.item.Item
import net.minecraft.util.Identifier
import tf.bug.monadmachines.MONADMACHINES_ITEMGROUP

object ProgramCardItem : Item(FabricItemSettings().group(MONADMACHINES_ITEMGROUP).maxCount(1)) {

    val id = Identifier("monadmachines", "program_card")

}
