package tf.bug.monadmachines.item

import net.fabricmc.loader.api.FabricLoader
import net.minecraft.client.item.TooltipContext
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.text.Text
import net.minecraft.text.TextColor
import net.minecraft.text.TranslatableText
import net.minecraft.util.*
import net.minecraft.world.World
import tf.bug.monadmachines.MONADMACHINES_ITEMGROUP
import vazkii.patchouli.api.PatchouliAPI

object MonadMachinesManualItem : Item(Settings().group(MONADMACHINES_ITEMGROUP)) {

    val id = Identifier("monadmachines", "manual")

    fun getEdition(): Text {
        return if(FabricLoader.getInstance().isModLoaded("patchouli")) {
            PatchouliAPI.get().getSubtitle(id)
        } else {
            val text = TranslatableText("monadmachines.no_patchouli_tooltip")
            text.style = text.style.withColor(TextColor.fromFormatting(Formatting.RED))
            text
        }
    }

    override fun appendTooltip(
        stack: ItemStack,
        world: World?,
        tooltip: MutableList<Text>,
        context: TooltipContext
    ) {
        super.appendTooltip(stack, world, tooltip, context)

        tooltip.add(getEdition().shallowCopy().formatted(Formatting.GRAY))
    }

    override fun use(world: World, user: PlayerEntity, hand: Hand): TypedActionResult<ItemStack> {
        val itemStack = user.getStackInHand(hand)

        if(user is ServerPlayerEntity) {
            if(FabricLoader.getInstance().isModLoaded("patchouli")) {
                PatchouliAPI.get().openBookGUI(user, id)
            } else {
                val text = TranslatableText("monadmachines.no_patchouli_message")
                text.style = text.style.withColor(TextColor.fromFormatting(Formatting.RED))
                user.sendSystemMessage(text, Util.NIL_UUID)
            }
        }

        return TypedActionResult(ActionResult.SUCCESS, itemStack)
    }

}