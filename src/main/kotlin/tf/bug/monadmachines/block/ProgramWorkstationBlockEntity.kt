package tf.bug.monadmachines.block

import net.minecraft.block.BlockState
import net.minecraft.block.InventoryProvider
import net.minecraft.block.entity.BlockEntity
import net.minecraft.block.entity.BlockEntityType
import net.minecraft.block.entity.LootableContainerBlockEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.player.PlayerInventory
import net.minecraft.inventory.Inventories
import net.minecraft.inventory.Inventory
import net.minecraft.inventory.SidedInventory
import net.minecraft.item.ItemStack
import net.minecraft.nbt.CompoundTag
import net.minecraft.screen.NamedScreenHandlerFactory
import net.minecraft.screen.ScreenHandler
import net.minecraft.screen.ScreenHandlerContext
import net.minecraft.text.Text
import net.minecraft.text.TranslatableText
import net.minecraft.util.collection.DefaultedList
import net.minecraft.util.math.BlockPos
import net.minecraft.world.WorldAccess
import tf.bug.monadmachines.item.ProgramCardItem

class ProgramWorkstationBlockEntity : LootableContainerBlockEntity(type) {

    private var card: DefaultedList<ItemStack> = DefaultedList.ofSize(1, ItemStack.EMPTY)

    companion object {
        val type: BlockEntityType<ProgramWorkstationBlockEntity> =
            BlockEntityType.Builder.create({ ProgramWorkstationBlockEntity() }, ProgramWorkstationBlock).build(null)
    }

    override fun createScreenHandler(syncId: Int, inv: PlayerInventory): ScreenHandler {
        return ProgramWorkstationGuiDescription(syncId, inv, ScreenHandlerContext.create(world, pos))
    }

    override fun getContainerName(): Text {
        return TranslatableText(cachedState.block.translationKey)
    }

    override fun getInvStackList(): DefaultedList<ItemStack> {
        return card
    }

    override fun setInvStackList(list: DefaultedList<ItemStack>) {
        card = list
    }

    override fun size(): Int {
        return 1
    }

    override fun isValid(slot: Int, stack: ItemStack): Boolean {
        return stack.isEmpty || stack.item is ProgramCardItem
    }

    override fun fromTag(state: BlockState, tag: CompoundTag) {
        super.fromTag(state, tag)
        Inventories.fromTag(tag, card)
    }

    override fun toTag(tag: CompoundTag): CompoundTag {
        Inventories.toTag(tag, card)
        return super.toTag(tag)
    }

}
