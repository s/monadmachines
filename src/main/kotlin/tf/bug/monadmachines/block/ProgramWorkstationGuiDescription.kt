package tf.bug.monadmachines.block

import io.github.cottonmc.cotton.gui.SyncedGuiDescription
import io.github.cottonmc.cotton.gui.widget.WGridPanel
import io.github.cottonmc.cotton.gui.widget.WItemSlot
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry
import net.minecraft.entity.player.PlayerInventory
import net.minecraft.screen.ScreenHandlerContext
import net.minecraft.screen.ScreenHandlerType

class ProgramWorkstationGuiDescription(syncId: Int, playerInventory: PlayerInventory, context: ScreenHandlerContext) : SyncedGuiDescription(type, syncId, playerInventory, getBlockInventory(context, inventorySize), getBlockPropertyDelegate(context)) {

    init {
        val root = WGridPanel()
        setRootPanel(root)
        root.setSize(198, 288)

        val itemSlot = WItemSlot(blockInventory, 0, 1, 1, true)
        itemSlot.setFilter { stack -> blockInventory.isValid(0, stack) }
        root.add(itemSlot, 0, 11)
        itemSlot.setLocation(6, 11 * 18 + 15)

        root.add(this.createPlayerInventoryPanel(), 2, 11)

        root.validate(this)
    }

    companion object {
        const val inventorySize: Int = 1

        val type: ScreenHandlerType<ProgramWorkstationGuiDescription> =
            ScreenHandlerRegistry.registerSimple(ProgramWorkstationBlock.id) { syncId: Int, playerInventory: PlayerInventory ->
                ProgramWorkstationGuiDescription(
                    syncId,
                    playerInventory,
                    ScreenHandlerContext.EMPTY
                )
            }
    }

}
