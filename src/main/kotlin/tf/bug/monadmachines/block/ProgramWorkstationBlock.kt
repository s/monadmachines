package tf.bug.monadmachines.block

import net.fabricmc.fabric.api.`object`.builder.v1.block.FabricBlockSettings
import net.minecraft.block.BlockRenderType
import net.minecraft.block.BlockState
import net.minecraft.block.BlockWithEntity
import net.minecraft.block.Material
import net.minecraft.block.entity.BlockEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.BlockItem
import net.minecraft.item.Item
import net.minecraft.screen.ScreenHandler
import net.minecraft.util.ActionResult
import net.minecraft.util.Hand
import net.minecraft.util.Identifier
import net.minecraft.util.ItemScatterer
import net.minecraft.util.hit.BlockHitResult
import net.minecraft.util.math.BlockPos
import net.minecraft.world.BlockView
import net.minecraft.world.World
import tf.bug.monadmachines.MONADMACHINES_ITEMGROUP

object ProgramWorkstationBlock : BlockWithEntity(FabricBlockSettings.of(Material.METAL)) {

    val id = Identifier("monadmachines", "program_workstation")
    val item = BlockItem(this, Item.Settings().group(MONADMACHINES_ITEMGROUP))
    val entityType = ProgramWorkstationBlockEntity.type

    override fun createBlockEntity(world: BlockView?): BlockEntity {
        return ProgramWorkstationBlockEntity()
    }

    override fun getRenderType(state: BlockState?): BlockRenderType {
        return BlockRenderType.MODEL
    }

    override fun onUse(
        state: BlockState,
        world: World,
        pos: BlockPos,
        player: PlayerEntity?,
        hand: Hand?,
        hit: BlockHitResult?
    ): ActionResult {
        if(!world.isClient) {
            val screenHandlerFactory = state.createScreenHandlerFactory(world, pos)

            if(screenHandlerFactory != null && player != null) {
                player.openHandledScreen(screenHandlerFactory)
            }
        }
        return ActionResult.SUCCESS
    }

    override fun onStateReplaced(
        state: BlockState,
        world: World,
        pos: BlockPos,
        newState: BlockState,
        moved: Boolean
    ) {
        if(state.block != newState.block) {
            val blockEntity = world.getBlockEntity(pos)
            if(blockEntity is ProgramWorkstationBlockEntity) {
                ItemScatterer.spawn(world, pos, blockEntity)
                world.updateComparators(pos, this)
            }
            super.onStateReplaced(state, world, pos, newState, moved)
        }
    }

    override fun hasComparatorOutput(state: BlockState): Boolean {
        return true
    }

    override fun getComparatorOutput(state: BlockState, world: World, pos: BlockPos): Int {
        return ScreenHandler.calculateComparatorOutput(world.getBlockEntity(pos))
    }

}
