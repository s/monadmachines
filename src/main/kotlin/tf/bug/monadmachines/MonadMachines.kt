package tf.bug.monadmachines

import net.fabricmc.api.EnvType
import net.fabricmc.api.Environment
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder
import net.fabricmc.fabric.api.client.screenhandler.v1.ScreenRegistry
import net.minecraft.entity.player.PlayerInventory
import net.minecraft.item.ItemGroup
import net.minecraft.item.ItemStack
import net.minecraft.text.Text
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import tf.bug.monadmachines.block.ProgramWorkstationBlock
import tf.bug.monadmachines.block.ProgramWorkstationScreen
import tf.bug.monadmachines.block.ProgramWorkstationGuiDescription
import tf.bug.monadmachines.item.MonadMachinesManualItem
import tf.bug.monadmachines.item.ProgramCardItem

val MONADMACHINES_ITEMGROUP: ItemGroup = FabricItemGroupBuilder.build(
    Identifier("monadmachines", "group")
) { ItemStack(ProgramWorkstationBlock) }

@Suppress("unused")
fun init() {
    Registry.register(Registry.ITEM, MonadMachinesManualItem.id, MonadMachinesManualItem)

    Registry.register(Registry.BLOCK, ProgramWorkstationBlock.id, ProgramWorkstationBlock)
    Registry.register(Registry.ITEM, ProgramWorkstationBlock.id, ProgramWorkstationBlock.item)
    Registry.register(Registry.BLOCK_ENTITY_TYPE, ProgramWorkstationBlock.id, ProgramWorkstationBlock.entityType)

    Registry.register(Registry.ITEM, ProgramCardItem.id, ProgramCardItem)
}

@Suppress("unused")
@Environment(EnvType.CLIENT)
fun clientInit() {
    ScreenRegistry.register(ProgramWorkstationGuiDescription.type) {
            handler: ProgramWorkstationGuiDescription, inventory: PlayerInventory, title: Text ->
            ProgramWorkstationScreen(handler, inventory.player, title)
    }
}
