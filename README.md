# MonadMachines

A computer mod that aims to teach functional programming concepts through
a tutorial book and a simple visual interface.

Depends on:
- [Fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api)
- [Fabric Language Kotlin](https://www.curseforge.com/minecraft/mc-mods/fabric-language-kotlin)
- [LibGui](https://github.com/CottonMC/LibGui)

Recommended:
- [Patchouli](https://www.curseforge.com/minecraft/mc-mods/patchouli-fabric):
  The mod uses Patchouli for its documentation. **Don't leave out Patchouli
  unless you know what you're doing.**
  
Suggested:
- [Mod Menu](https://modrinth.com/mod/modmenu): Shows mod information.
- [Polymorph](https://www.curseforge.com/minecraft/mc-mods/polymorph-fabric):
  Adds a recipe conflict resolver. Certain crafting recipes in MonadMachines
  may conflict with other mods.
